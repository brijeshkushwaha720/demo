﻿using System;
using CommonDAL;
using IRepositoryDAL;
using System.Data;
using IModel;
using MySql.Data.MySqlClient;

namespace AdoDotnetDAL
{
    public abstract class TemplateAdo<AnyType>: AbstractDal<AnyType>
    {
        protected MySqlConnection objConn = null;
        protected MySqlCommand objCommand = null;
        public TemplateAdo(string _ConnectionString) : base(_ConnectionString)
        {

        }

        private void Open()
        {
            objConn = new MySqlConnection(ConnectionString);
            objConn.Open();
            objCommand = new MySqlCommand();
            objCommand.Connection = objConn;
        }

        protected abstract void ExecuteCommand(AnyType obj);

        private void Close()
        {
            objConn.Close();
        }

        public void Execute(AnyType obj) // Fixed Sequence Insert
        {
            Open();
            ExecuteCommand(obj);
            Close();
        }

        public override void Save()
        {
            foreach(AnyType o in AnyTypes)
            {
                Execute(o);
            }
        }
    }

    public class DomainDAL : TemplateAdo<IDomain>,IDal<IDomain>
    {
        public DomainDAL(string _ConnectionString) : base(_ConnectionString)
        {

        }

        protected override void ExecuteCommand(IDomain obj)
        {
            objCommand.CommandText = "insert into domain_settings(" +
                                            "domain_name," +
                                            "is_active," +
                                            "created_at,updated_at)" +
                                            "values('" + obj.domain_name + "'," +
                                            obj.is_active + ",'" +
                                            obj.created_at + "','" +
                                            obj.updated_at + "')";
            objCommand.ExecuteNonQuery();
        }
    }
}
