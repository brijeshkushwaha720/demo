﻿using System;
using System.Collections.Generic;

namespace IRepositoryDAL
{
    public interface IDal<AnyType>
    {
        void Add(AnyType obj); 
        void Update(AnyType obj);
        List<AnyType> Search();
        void Save(); 
    }
}
