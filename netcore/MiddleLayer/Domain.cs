﻿using System;
using IModel;

namespace MiddleLayer
{
    public class Domain : IDomain
    {
        public long? domain_id { get; set; }
        public string domain_name { get; set; }
        public bool is_active { get ; set ; }
        public string created_at { get ; set; }
        public string updated_at { get; set; }

        public Domain()
        {
            domain_id = 0;
            domain_name = "";
            is_active = false;
            created_at = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"); 
            updated_at = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss");
        }
    }
}
