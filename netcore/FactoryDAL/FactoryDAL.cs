﻿using Microsoft.Practices.Unity;
using System;
using AdoDotnetDAL;
using CommonDAL;
using IRepositoryDAL;
using IModel;

namespace FactoryDAL
{
    public class FactoryDataAccessLayer<AnyType>
    {
        private static IUnityContainer ObjectsofOurProjects = null;

        public static AnyType Create(string Type)
        {
            if (ObjectsofOurProjects == null)
            {
                ObjectsofOurProjects = new UnityContainer();
                ObjectsofOurProjects.RegisterType<IDal<IDomain>, DomainDAL>("ADODal");
            }

            return ObjectsofOurProjects.Resolve<AnyType>(Type,
                               new ResolverOverride[]
                               {
                                       new ParameterOverride("_ConnectionString",
                                        @"Server=127.0.0.1;Database=samooh;Uid=root;Pwd=bri@720;")
                               }); ;
        }

    }
}
