﻿using System;

namespace IModel
{
    public interface IDomain
    {
        Int64? domain_id { get; set; }
        string domain_name { get; set; }
        bool is_active { get; set; }
        string created_at { get; set; }
        string updated_at { get; set; }
    }
}
